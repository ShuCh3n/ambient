<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
	protected $table = "game";
    protected $hidden = ['created_at', 'updated_at'];
}
