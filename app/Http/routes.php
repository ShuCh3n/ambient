<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('game');
});

$app->get('newgamecheck', ["uses" => "PageController@newgamecheck"]);
$app->get('newgame', ["uses" => "PageController@newgame"]);
$app->get('finish_first_player', ["uses" => "PageController@fisish_first_player"]);
$app->get('finish_game', ["uses" => "PageController@finish_game"]);
$app->get('score', ["uses" => "PageController@score"]);