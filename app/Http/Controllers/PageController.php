<?php

namespace App\Http\Controllers;

use App\Game;

class PageController extends Controller
{
    public function newgamecheck()
    {
        $active_game = Game::where('active', 1)->first();

        if($active_game)
        {
            return "1";
        }
        
        return "0";
    }

    public function fisish_first_player()
    {
        $active_game = Game::where('active', 1)->first();

        if($active_game)
        {
            $active_game->player_one_finish = true;
            $active_game->save();
        }
    }

    public function finish_game()
    {
        $active_game = Game::where('active', 1)->first();

        if($active_game)
        {
            $active_game->active = false;
            $active_game->save();

            return $active_game;
        }
    }

    public function score()
    {
        $active_game = Game::where('active', 1)->first();

        if($active_game)
        {
            if(!$active_game->player_one_finish)
            {
                $active_game->player_one_score = $active_game->player_one_score + 1;
                $active_game->save();
                return "score";
            }
            
            $active_game->player_two_score = $active_game->player_two_score + 1;
            $active_game->save();
            return "score";
        }
    }

    public function newgame()
    {
        $active_game = Game::where('active', 1)->first();
        if(!$active_game)
        {
            $game = new Game;
            $game->active = true;
            $game->player_one_score = 0;
            $game->player_one_finish = false;
            $game->player_two_score = 0;
            $game->save();
        }
        
    }
}
