<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ambient Game</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <script>
    var interval;

	function checkNewGame() {
	  $.get('newgamecheck', function(result){
	  	if(result == 1)
	  	{
	  		startGame();
	  	}
	  });
	}

	function startGame(){
		clearInterval(interval);

		display = document.querySelector('#time');
		startPlayerOne(10, display);
	}

	function startPlayerOne(duration, display) {
		$(".start_game_text").hide();
		$(".score").hide();
		player = document.querySelector('#player');
		player.textContent = "Player 1";

	    var timer = duration, minutes, seconds;
	    var player_one_interval = setInterval(function () {
	        minutes = parseInt(timer / 60, 10)
	        seconds = parseInt(timer % 60, 10);

	        minutes = minutes < 10 ? "0" + minutes : minutes;
	        seconds = seconds < 10 ? "0" + seconds : seconds;

	        display.textContent = minutes + ":" + seconds;

	        if (--timer < 0) {
	            timer = duration;
	            clearInterval(player_one_interval);
	            $.get('finish_first_player');
	            return startPlayerTwo(10, display)
	        }
	    }, 1000);
	}

	function startPlayerTwo(duration, display) {
		player = document.querySelector('#player');
		player.textContent = "Player 2";

	    var timer = duration, minutes, seconds;
	    var player_two_interval = setInterval(function () {
	        minutes = parseInt(timer / 60, 10)
	        seconds = parseInt(timer % 60, 10);

	        minutes = minutes < 10 ? "0" + minutes : minutes;
	        seconds = seconds < 10 ? "0" + seconds : seconds;

	        display.textContent = minutes + ":" + seconds;

	        if (--timer < 0) {
	            timer = duration;
	            clearInterval(player_two_interval);
	            return finishGame();
	        }
	    }, 1000);
	}

	function finishGame()
	{
		$.get('finish_game', function(score){
			document.querySelector('#player_one_score').textContent = score.player_one_score;
			document.querySelector('#player_two_score').textContent = score.player_two_score;
		});

		$(".start_game_text").show();
		$(".score").show();
		document.querySelector('#player').textContent = ""
		document.querySelector('#time').textContent = ""
		interval = setInterval(checkNewGame, 500);
	}

	window.onload = function () {
		$(".score").hide();
		interval = setInterval(checkNewGame, 500);
	};
	</script>

	<style>
	body{
		font-size: 50px;
	}
	</style>
  </head>
  <body>
  	<div class="container">
  		<div class="col-md-12 text-center">
	  		<div class="start_game_text">Press the yellow button to start the game!</div>
	  		<div id="player"></div>
	  		<span id="time"></span>
	  	</div>

	  	<div class="col-md-6 text-center score">
	  		<div>Player 1</div>
	  		<div id="player_one_score"></div>
	  	</div>

	  	<div class="col-md-6 text-center score">
	  		<div>Player 2</div>
	  		<div id="player_two_score"></div>
	  	</div>
	    
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  </body>
</html>

